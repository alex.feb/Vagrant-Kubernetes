class gcloud{

}

class gcloud::download{
	exec  { 'gcloud_download':
		command => 'wget -qO- https://dl.google.com/dl/cloudsdk/channels/rapid/google-cloud-sdk.tar.gz | tar xvz -C /tmp',
		path => [ '/usr/bin/', '/bin/' ],
	}
}

class gcloud::install {
	exec { 'gcloud install':
		command =>'/tmp/google-cloud-sdk/install.sh --command-completion=true --rc-path=/home/vagrant/.bashrc',
		 path => [ '/usr/bin/', '/bin/' ],
	     	 require => Class['gcloud::download'],
}
}




