# Ensure apt is present, and adds the ability to update the repositories when the apt::update method is invoked.
class apt {
	  package { 'apt':
                name            => 'apt',
                ensure          => present,
        }
}

class apt::update {
	exec { "apt-get update":
	path => ['/usr/bin', '/usr/sbin'],
	}
}



