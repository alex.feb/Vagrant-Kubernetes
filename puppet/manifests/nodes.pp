node default {
        include base
        include apt 
        include apt::update 
}

node 'devk8s01.local' inherits default {
	include gcloud 
	include gcloud::download 
	include gcloud::install 
}
